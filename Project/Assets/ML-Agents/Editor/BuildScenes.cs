using System;
using UnityEditor;
using UnityEngine;
using UnityEditor.Build.Reporting;

namespace Unity.MLAgents
{
    public class SceneBuilder
    {
        static void BuildTennis ()
        {
            string[] scenes = { "Assets/ML-Agents/Examples/Tennis/Scenes/Tennis.unity" };
            var buildResult = BuildPipeline.BuildPlayer(
                    scenes,
                    "Environments/Tennis.x86_64",
                    BuildTarget.StandaloneLinux64,
                    BuildOptions.None
            );
            Console.WriteLine("Environment saved to: {0}", buildResult.summary.outputPath);
        }

        static void BuildBasic ()
        {
            string[] scenes = { "Assets/ML-Agents/Examples/Basic/Scenes/Basic.unity" };
            var buildResult = BuildPipeline.BuildPlayer(
                    scenes,
                    "Environments/Basic.x86_64",
                    BuildTarget.StandaloneLinux64,
                    BuildOptions.None
            );
            Console.WriteLine("Environment saved to: {0}", buildResult.summary.outputPath);
        }
    }
}
